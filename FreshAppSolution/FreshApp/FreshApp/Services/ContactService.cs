﻿using Bogus;
using FreshApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace FreshApp.Services
{
    public class ContactService : IContactService
    {
        public async Task<ObservableCollection<Contact>> GetDataAsync()
        {
            await Task.Delay(500);

            List<Contact> FakeContacts = new Faker<Contact>()
               .RuleFor(o => o.Name, f => f.Name.FullName())
               .RuleFor(o => o.Phone, f => f.Phone.PhoneNumber())
               .Generate(25);


            foreach (Contact item in FakeContacts)
            {
                int index = new Random().Next(1, 11);
                item.Photo = $"photo{index}.png";
            }

            return new ObservableCollection<Contact>(FakeContacts);
        }
    }
}
