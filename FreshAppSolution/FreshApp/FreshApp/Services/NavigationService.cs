﻿using FreshApp.Enums;
using FreshApp.ViewModels;
using FreshMvvm;

namespace FreshApp.Services
{
    public class NavigationService : INavigationService
    {
        public void SwitchNavigation(NavigationStacks navigationStack, FreshBasePageModel page)
        {
            switch (navigationStack)
            {
                case NavigationStacks.Authentication:
                    var loginPage = FreshPageModelResolver.ResolvePageModel<LoginViewModel>();
                    var authentifactionNavigation = new FreshNavigationContainer(loginPage, navigationStack.ToString());
                    page.CoreMethods.SwitchOutRootNavigation(navigationStack.ToString());
                    break;
                case NavigationStacks.Main:
                    var mainPage = FreshPageModelResolver.ResolvePageModel<MainViewModel>();
                    var mainNavigation = new FreshNavigationContainer(mainPage, navigationStack.ToString());
                    page.CoreMethods.SwitchOutRootNavigation(navigationStack.ToString());
                    break;
                case NavigationStacks.Tabbed:
                    var Tabbed = new FreshTabbedNavigationContainer(navigationStack.ToString());
                    Tabbed.AddTab<MainViewModel>("Contacte", null);
                    Tabbed.AddTab<MainViewModel>("About", null);
                    page.CoreMethods.SwitchOutRootNavigation(navigationStack.ToString());
                    break;
                default:
                    break;
            }
        }
    }
}
