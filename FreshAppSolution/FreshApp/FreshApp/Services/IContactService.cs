﻿using FreshApp.Models;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace FreshApp.Services
{
    public interface IContactService
    {
        Task<ObservableCollection<Contact>> GetDataAsync();
    }
}
