﻿using FreshApp.Enums;
using FreshMvvm;

namespace FreshApp.Services
{
    public interface INavigationService
    {
        void SwitchNavigation(NavigationStacks navigationStack, FreshBasePageModel page);
    }
}
