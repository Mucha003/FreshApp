﻿using Acr.UserDialogs;
using FreshApp.Models;
using FreshApp.Services;
using FreshMvvm;
using System;
using System.Collections.ObjectModel;

namespace FreshApp.ViewModels
{
    public class MainViewModel : FreshBasePageModel
    {
        private Contact _selectedContact;
        private readonly IContactService _contactService;
        private readonly IUserDialogs _userDialogs;

        //RaisePropertyChanged() a ete remplacé par fody
        //RaisePropertyChanged(); // car FreshBasePageModel herite deja avec INotifuPropertyChange
        public ObservableCollection<Contact> Contacts { get; set; }
        public Contact SelectedContact
        {
            get => _selectedContact;
            set
            {
                _selectedContact = value;
                // navigation entre page, ici VM car freshMvvm travaille avec pour afficher les pages
                // CoreMethods: saut d'un VM a un Autre
                CoreMethods.PushPageModel<ContactDetailViewModel>(value); // en lui envoi notre contact a une autre VM.

                //// ---------------    Pile de Nvigation    ---------------   
                // Creation d'un nouveau container de Navigation.

                //var contacteatils = FreshPageModelResolver.ResolvePageModel<ContactDetailViewModel>(value);
                //var basicNavContainer = new FreshNavigationContainer(contacteatils, "ContactNavPageNameStack");
                //CoreMethods.PushNewNavigationServiceModal(basicNavContainer, contacteatils.GetModel());

                //var tabbed = new FreshTabbedNavigationContainer("NameStackPage");
                //tabbed.AddTab<ContactDetailViewModel>("Detail", null, value);
                //tabbed.AddTab<AboutViewModel>("About", null);
                //CoreMethods.PushNewNavigationServiceModal(tabbed);

                //var masterDetail = new FreshMasterDetailNavigationContainer("NameStackPage");
                //masterDetail.Init("Menu");
                //masterDetail.AddPage<ContactDetailViewModel>("Detail", value);
                //masterDetail.AddPage<AboutViewModel>("About", null);
                //CoreMethods.PushNewNavigationServiceModal(masterDetail);
                //// ---------------    Pile de Nvigation    ---------------   
            }
        }



        public MainViewModel(IContactService ContactService, IUserDialogs UserDialogs)
        {
            _contactService = ContactService;
            _userDialogs = UserDialogs;
        }



        // est appele kand a page est chargé.
        // s'sexecute lors de la creation du VM. Remove Ctor.
        public override async void Init(object initData)
        {
            _userDialogs.ShowLoading("Loading");
            Contacts = await _contactService.GetDataAsync();
            _userDialogs.HideLoading();
        }


        // renvoi la Data a un ViewModel Du quel En viens Prescedament. (pop vers push)
        // Ne s'execute QUE quand une page qui a ete push et puis pop lui renvoi des données.
        public override void ReverseInit(object returnedData)
        {
            base.ReverseInit(returnedData);
        }


        // s'exec quand une page est entraint d'apparaitre
        protected override void ViewIsAppearing(object sender, EventArgs e)
        {
            base.ViewIsAppearing(sender, e);
        }


        // s'exec quand une page est entraint dispparaitre
        protected override void ViewIsDisappearing(object sender, EventArgs e)
        {
            base.ViewIsDisappearing(sender, e);
        }
    }
}
