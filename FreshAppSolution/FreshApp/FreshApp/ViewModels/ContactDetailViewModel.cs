﻿using FreshApp.Models;
using FreshApp.Services;
using FreshMvvm;
using System.Windows.Input;
using Xamarin.Forms;

namespace FreshApp.ViewModels
{
    public class ContactDetailViewModel : FreshBasePageModel
    {
        private readonly INavigationService _navigationService;
        public Contact Profile { get; set; }

        public ICommand ReturnCommand { get; set; }



        public ContactDetailViewModel(INavigationService NavigationService)
        {
            _navigationService = NavigationService;
        }



        // ici on Catch la Valuer envoyé d'un autre VM
        public override void Init(object initData)
        {
            ReturnCommand = new Command(ReturnExecute);

            if (initData is Contact)
            {
                Profile = initData as Contact;
            }
        }


        // Logout
        public void ReturnExecute()
        {
            // renvoi data ver la page prescedente.
            //await CoreMethods.PopPageModel(Profile);
            _navigationService.SwitchNavigation(Enums.NavigationStacks.Authentication, this);
        }
    }
}
