﻿using FreshApp.Services;
using FreshMvvm;
using System.Windows.Input;
using Xamarin.Forms;

namespace FreshApp.ViewModels
{
    public class LoginViewModel : FreshBasePageModel
    {
        public ICommand LoginCommand { get; set; }
        public readonly INavigationService _navigationService;


        public LoginViewModel(INavigationService NavigationService)
        {
            _navigationService = NavigationService;
        }

        public override void Init(object initData)
        {
            LoginCommand = new Command(Connection);
            base.Init(initData);
        }



        public void Connection()
        {
            //await CoreMethods.PushPageModel<MainViewModel>();

            // this: cette mm page
            _navigationService.SwitchNavigation(Enums.NavigationStacks.Main, this);
        }


    }
}
