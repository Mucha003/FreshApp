﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FreshApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactDetailPage : ContentPage
    {
        public ContactDetailPage()
        {
            InitializeComponent();
        }
    }
}