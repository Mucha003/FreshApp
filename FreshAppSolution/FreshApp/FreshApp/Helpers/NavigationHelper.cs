﻿using FreshApp.Services;

namespace FreshApp.Helpers
{
    public static class NavigationHelper
    {
        static INavigationService _navigationService;
        public static INavigationService Instance 
        {
            get
            {
                if (_navigationService == null)
                {
                    _navigationService = new NavigationService();
                    return _navigationService;
                }

                return _navigationService;
            }
            set => _navigationService = value;
        }
    }
}
