﻿using Acr.UserDialogs;
using FreshApp.Helpers;
using FreshApp.Services;
using FreshApp.ViewModels;
using FreshMvvm;
using Xamarin.Forms;

namespace FreshApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            ConfigureService();

            ////// Content Page
            ////MainPage = FreshPageModelResolver.ResolvePageModel<MainViewModel>();

            ////// Navigation Page
            Page InitPage = FreshPageModelResolver.ResolvePageModel<LoginViewModel>();
            MainPage = new FreshNavigationContainer(InitPage); // == new NavigationPage(InitPage)

            //////// Master Detail Page
            //FreshMasterDetailNavigationContainer masterDetail = new FreshMasterDetailNavigationContainer();
            //masterDetail.AddPage<MainViewModel>("Contacts");
            //masterDetail.AddPage<AboutViewModel>("About");

            //// Obligatoire, main page, (si nous avous un icone personalise o peut le mettre ici
            //masterDetail.Init("Menu"); 

            //MainPage = masterDetail;


            //////// Tab Page
            //var Tabbed = new FreshTabbedNavigationContainer();
            //// p1= Title,  p2= Icone 
            //Tabbed.AddTab<MainViewModel>("Contacte", null);
            //Tabbed.AddTab<MainViewModel>("About", null);
            //MainPage = Tabbed;



            //////// Charger multiple dasn un mm container.
            //var masterDetail = new MasterDetailPage();
            //var mainPage = FreshPageModelResolver
            //                .ResolvePageModel<MainViewModel>();
            //mainPage.Title = "Contacts";

            //var masterPage = new FreshNavigationContainer(mainPage, "MasterPageNameStack");
            //masterPage.Title = "Menu";
            //masterDetail.Master = masterPage;

            //// page principale a montrer dans le master detail
            //var about = FreshPageModelResolver
            //                .ResolvePageModel<AboutViewModel>();
            //about.Title = "About";
            //var detailPage = new FreshNavigationContainer(about, "DetailPageNameStack");
            //masterDetail.Detail = detailPage;

            //mainPage = masterDetail;
        }


        public void ConfigureService()
        {
            FreshIOC.Container.Register<IContactService, ContactService>();
            FreshIOC.Container.Register<IUserDialogs>(UserDialogs.Instance);
            FreshIOC.Container.Register<INavigationService>(NavigationHelper.Instance);
        }


        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
